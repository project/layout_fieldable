<?php

namespace Drupal\layout_fieldable\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\layout_fieldable\Entity\LayoutFieldableEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LayoutFieldableEntityController.
 *
 *  Returns responses for Layout Fieldable Entity routes.
 */
class LayoutFieldableEntityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Layout Fieldable Entity revision.
   *
   * @param int $layout_fieldable_entity_revision
   *   The Layout Fieldable Entity revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($layout_fieldable_entity_revision) {
    $layout_fieldable_entity = $this->entityTypeManager()->getStorage('layout_fieldable_entity')
      ->loadRevision($layout_fieldable_entity_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('layout_fieldable_entity');

    return $view_builder->view($layout_fieldable_entity);
  }

  /**
   * Page title callback for a Layout Fieldable Entity revision.
   *
   * @param int $layout_fieldable_entity_revision
   *   The Layout Fieldable Entity revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($layout_fieldable_entity_revision) {
    $layout_fieldable_entity = $this->entityTypeManager()->getStorage('layout_fieldable_entity')
      ->loadRevision($layout_fieldable_entity_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $layout_fieldable_entity->label(),
      '%date' => $this->dateFormatter->format($layout_fieldable_entity->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Layout Fieldable Entity.
   *
   * @param \Drupal\layout_fieldable\Entity\LayoutFieldableEntityInterface $layout_fieldable_entity
   *   A Layout Fieldable Entity object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(LayoutFieldableEntityInterface $layout_fieldable_entity) {
    $account = $this->currentUser();
    $layout_fieldable_entity_storage = $this->entityTypeManager()->getStorage('layout_fieldable_entity');

    $langcode = $layout_fieldable_entity->language()->getId();
    $langname = $layout_fieldable_entity->language()->getName();
    $languages = $layout_fieldable_entity->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $layout_fieldable_entity->label()]) : $this->t('Revisions for %title', ['%title' => $layout_fieldable_entity->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all layout fieldable entity revisions") || $account->hasPermission('administer layout fieldable entity entities')));
    $delete_permission = (($account->hasPermission("delete all layout fieldable entity revisions") || $account->hasPermission('administer layout fieldable entity entities')));

    $rows = [];

    $vids = $layout_fieldable_entity_storage->revisionIds($layout_fieldable_entity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\layout_fieldable\LayoutFieldableEntityInterface $revision */
      $revision = $layout_fieldable_entity_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $layout_fieldable_entity->getRevisionId()) {
          $link = $this->l($date, new Url('entity.layout_fieldable_entity.revision', [
            'layout_fieldable_entity' => $layout_fieldable_entity->id(),
            'layout_fieldable_entity_revision' => $vid,
          ]));
        }
        else {
          $link = $layout_fieldable_entity->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.layout_fieldable_entity.translation_revert', [
                'layout_fieldable_entity' => $layout_fieldable_entity->id(),
                'layout_fieldable_entity_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.layout_fieldable_entity.revision_revert', [
                'layout_fieldable_entity' => $layout_fieldable_entity->id(),
                'layout_fieldable_entity_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.layout_fieldable_entity.revision_delete', [
                'layout_fieldable_entity' => $layout_fieldable_entity->id(),
                'layout_fieldable_entity_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['layout_fieldable_entity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
