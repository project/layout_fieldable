<?php

namespace Drupal\layout_fieldable;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\layout_fieldable\Entity\LayoutFieldableEntityInterface;

/**
 * Defines the storage handler class for Layout Fieldable Entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Layout Fieldable Entity entities.
 *
 * @ingroup layout_fieldable
 */
interface LayoutFieldableEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Layout Fieldable Entity revision IDs for a specific Layout Fieldable Entity.
   *
   * @param \Drupal\layout_fieldable\Entity\LayoutFieldableEntityInterface $entity
   *   The Layout Fieldable Entity entity.
   *
   * @return int[]
   *   Layout Fieldable Entity revision IDs (in ascending order).
   */
  public function revisionIds(LayoutFieldableEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Layout Fieldable Entity author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Layout Fieldable Entity revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\layout_fieldable\Entity\LayoutFieldableEntityInterface $entity
   *   The Layout Fieldable Entity entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(LayoutFieldableEntityInterface $entity);

  /**
   * Unsets the language for all Layout Fieldable Entity with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
