<?php

namespace Drupal\layout_fieldable;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\layout_fieldable\Entity\LayoutFieldableEntityInterface;

/**
 * Defines the storage handler class for Layout Fieldable Entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Layout Fieldable Entity entities.
 *
 * @ingroup layout_fieldable
 */
class LayoutFieldableEntityStorage extends SqlContentEntityStorage implements LayoutFieldableEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(LayoutFieldableEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {layout_fieldable_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {layout_fieldable_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(LayoutFieldableEntityInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {layout_fieldable_entity_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('layout_fieldable_entity_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
