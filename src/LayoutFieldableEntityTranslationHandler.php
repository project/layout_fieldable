<?php

namespace Drupal\layout_fieldable;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for layout_fieldable_entity.
 */
class LayoutFieldableEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
