<?php

namespace Drupal\layout_fieldable;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\layout_fieldable\Entity\LayoutFieldableEntityType;

/**
 * Provides a listing of Layout Fieldable Entity type entities.
 */
class LayoutFieldableEntityTypeListBuilder extends ConfigEntityListBuilder {
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Layout');
    $header['id'] = $this->t('Machine name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    // You probably want a few more properties here...
    return $row + parent::buildRow($entity);
  }
  /**
   * {@inheritdoc}
   */
  public function render() {
    $render = parent::render();
    $render['table']['#rows'] = $this->buildRows();
    return $render;
  }

  public function buildRows() {
    $layout = \Drupal::service('plugin.manager.core.layout');
    /** @var \Drupal\Core\Layout\LayoutDefinition[] $definitions */
    $layout_definitions = $layout->getDefinitions();
    $rows = [];
    /** @var \Drupal\Core\Layout\LayoutDefinition[] $layout_definition */
    foreach ($layout_definitions as $layout_definition) {
      if ($layout_definition->id() === 'layout_builder_blank'){
        continue;
      }

      $id = $layout_definition->id();
      $layout_fieldable_entity_type = $id ? LayoutFieldableEntityType::load($id) : FALSE;

      $row['label'] = [
        'data' => $layout_definition->getLabel(),
      ];

      $row['id'] = [
        'data' => $id,
      ];

      if ($layout_fieldable_entity_type) {
        $row['operations']['data'] = $this->buildOperations($layout_fieldable_entity_type);
      }
      else {
        $row['operations']['data'] = [
          '#type' => 'operations',
          '#links' => [
            'enable_fields' => [
              'title' => $this->t('Enable Fields'),
              'url' => Url::fromRoute('layout_fieldable.layout_enable_fields', ['layout_id' => $id]),
            ],
          ],
        ];
      }
      //
      $rows[$id] = $row;
    }
    return $rows;
  }
}
