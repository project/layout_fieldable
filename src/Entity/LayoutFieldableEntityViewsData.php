<?php

namespace Drupal\layout_fieldable\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Layout Fieldable Entity entities.
 */
class LayoutFieldableEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
