<?php

namespace Drupal\layout_fieldable\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Layout Fieldable Entity entities.
 *
 * @ingroup layout_fieldable
 */
interface LayoutFieldableEntityInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Layout Fieldable Entity name.
   *
   * @return string
   *   Name of the Layout Fieldable Entity.
   */
  public function getName();

  /**
   * Sets the Layout Fieldable Entity name.
   *
   * @param string $name
   *   The Layout Fieldable Entity name.
   *
   * @return \Drupal\layout_fieldable\Entity\LayoutFieldableEntityInterface
   *   The called Layout Fieldable Entity entity.
   */
  public function setName($name);

  /**
   * Gets the Layout Fieldable Entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Layout Fieldable Entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Layout Fieldable Entity creation timestamp.
   *
   * @param int $timestamp
   *   The Layout Fieldable Entity creation timestamp.
   *
   * @return \Drupal\layout_fieldable\Entity\LayoutFieldableEntityInterface
   *   The called Layout Fieldable Entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Layout Fieldable Entity revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Layout Fieldable Entity revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\layout_fieldable\Entity\LayoutFieldableEntityInterface
   *   The called Layout Fieldable Entity entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Layout Fieldable Entity revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Layout Fieldable Entity revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\layout_fieldable\Entity\LayoutFieldableEntityInterface
   *   The called Layout Fieldable Entity entity.
   */
  public function setRevisionUserId($uid);

}
