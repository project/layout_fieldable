<?php

namespace Drupal\layout_fieldable\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Layout Fieldable Entity type entity.
 *
 * @ConfigEntityType(
 *   id = "layout_fieldable_entity_type",
 *   label = @Translation("Layout"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\layout_fieldable\LayoutFieldableEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\layout_fieldable\Form\LayoutFieldableEntityTypeForm",
 *       "edit" = "Drupal\layout_fieldable\Form\LayoutFieldableEntityTypeForm",
 *       "delete" = "Drupal\layout_fieldable\Form\LayoutFieldableEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\layout_fieldable\LayoutFieldableEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "layout_fieldable_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "layout_fieldable_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/layout_fieldable_entity_type/{layout_fieldable_entity_type}",
 *     "add-form" = "/admin/structure/layout_fieldable_entity_type/add",
 *     "edit-form" = "/admin/structure/layout_fieldable_entity_type/{layout_fieldable_entity_type}/edit",
 *     "delete-form" = "/admin/structure/layout_fieldable_entity_type/{layout_fieldable_entity_type}/delete",
 *     "collection" = "/admin/structure/layout_fieldable_entity_type"
 *   }
 * )
 */
class LayoutFieldableEntityType extends ConfigEntityBundleBase implements LayoutFieldableEntityTypeInterface {

  /**
   * The Layout Fieldable Entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Layout Fieldable Entity type label.
   *
   * @var string
   */
  protected $label;

}
