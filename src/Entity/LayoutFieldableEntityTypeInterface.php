<?php

namespace Drupal\layout_fieldable\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Layout Fieldable Entity type entities.
 */
interface LayoutFieldableEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
