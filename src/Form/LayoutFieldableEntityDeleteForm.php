<?php

namespace Drupal\layout_fieldable\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Layout Fieldable Entity entities.
 *
 * @ingroup layout_fieldable
 */
class LayoutFieldableEntityDeleteForm extends ContentEntityDeleteForm {


}
