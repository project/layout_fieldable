<?php

namespace Drupal\layout_fieldable\Form;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\layout_fieldable\Entity\LayoutFieldableEntityType;

/**
 * Builds the form to delete Layout Fieldable Entity type entities.
 */
class LayoutFieldableEnableLayoutFields extends ConfirmFormBase {

  public function getQuestion() {
    return $this->t('Are you sure you want to enable fields for the %name layout?', ['%name' => $this->layout->getLabel()]);
  }

  public function getCancelUrl() {
    return new Url('entity.layout_fieldable_entity_type.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Enable');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $layout_id = null) {
    $layout_plugin_manager = \Drupal::service('plugin.manager.core.layout');
    /** @var \Drupal\Core\Layout\LayoutDefinition[] $definition */
    $definition = $layout_plugin_manager->getDefinition($layout_id);
    $this->layout = $definition;
    $form = parent::buildForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $layout_fieldable_type = LayoutFieldableEntityType::create([
      'id' => $this->layout->id(),
      'label' => $this->layout->getLabel(),
    ]);
    $layout_fieldable_type->isNew();
    $layout_fieldable_type->save();

    $this->messenger()->addMessage(
      $this->t('enabled fields on the @label layout.', [
        '@label' => $this->layout->getLabel(),
      ])
    );

    $form_state->setRedirectUrl($this->getCancelUrl());
  }


  public function getFormId() {
    return 'layout_fieldable_enable_layout_fields';
  }

}
