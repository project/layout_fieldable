<?php

namespace Drupal\layout_fieldable\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class LayoutFieldableEntityTypeForm.
 */
class LayoutFieldableEntityTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $layout_fieldable_entity_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $layout_fieldable_entity_type->label(),
      '#description' => $this->t("Label for the Layout Fieldable Entity type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $layout_fieldable_entity_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\layout_fieldable\Entity\LayoutFieldableEntityType::load',
      ],
      '#disabled' => !$layout_fieldable_entity_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $layout_fieldable_entity_type = $this->entity;
    $status = $layout_fieldable_entity_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Layout Fieldable Entity type.', [
          '%label' => $layout_fieldable_entity_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Layout Fieldable Entity type.', [
          '%label' => $layout_fieldable_entity_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($layout_fieldable_entity_type->toUrl('collection'));
  }

}
