<?php

namespace Drupal\layout_fieldable\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Layout Fieldable Entity revision.
 *
 * @ingroup layout_fieldable
 */
class LayoutFieldableEntityRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The Layout Fieldable Entity revision.
   *
   * @var \Drupal\layout_fieldable\Entity\LayoutFieldableEntityInterface
   */
  protected $revision;

  /**
   * The Layout Fieldable Entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $layoutFieldableEntityStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->layoutFieldableEntityStorage = $container->get('entity_type.manager')->getStorage('layout_fieldable_entity');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'layout_fieldable_entity_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.layout_fieldable_entity.version_history', ['layout_fieldable_entity' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $layout_fieldable_entity_revision = NULL) {
    $this->revision = $this->LayoutFieldableEntityStorage->loadRevision($layout_fieldable_entity_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->LayoutFieldableEntityStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Layout Fieldable Entity: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    $this->messenger()->addMessage(t('Revision from %revision-date of Layout Fieldable Entity %title has been deleted.', ['%revision-date' => format_date($this->revision->getRevisionCreationTime()), '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.layout_fieldable_entity.canonical',
       ['layout_fieldable_entity' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {layout_fieldable_entity_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.layout_fieldable_entity.version_history',
         ['layout_fieldable_entity' => $this->revision->id()]
      );
    }
  }

}
