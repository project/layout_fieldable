<?php

namespace Drupal\layout_fieldable;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Layout Fieldable Entity entities.
 *
 * @ingroup layout_fieldable
 */
class LayoutFieldableEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Layout Fieldable Entity ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\layout_fieldable\Entity\LayoutFieldableEntity $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.layout_fieldable_entity.edit_form',
      ['layout_fieldable_entity' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
