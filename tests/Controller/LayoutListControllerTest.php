<?php

namespace Drupal\layout_fieldable\Tests;

use Drupal\simpletest\WebTestBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Layout\LayoutPluginManagerInterface;

/**
 * Provides automated tests for the layout_fieldable module.
 */
class LayoutListControllerTest extends WebTestBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Layout\LayoutPluginManagerInterface definition.
   *
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected $pluginManagerCoreLayout;


  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "layout_fieldable LayoutListController's controller functionality",
      'description' => 'Test Unit for module layout_fieldable and controller LayoutListController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests layout_fieldable functionality.
   */
  public function testLayoutListController() {
    // Check that the basic functions of module layout_fieldable.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
